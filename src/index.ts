import express from "express";
import createError from "http-errors";
import bodyParser from "body-parser";
import dotenv from "dotenv";
import path from "path";

dotenv.config();

const format = (file: number) => path
    .format({ dir: `${__dirname}/files`, base: `${file}.json` });
const memo = (number = 1) => {
    return (next: any = 0) => {
        if ( next > 1) {
            number = next > 5 ? 1 : next;
            return number;
        }
        number = number === 5 ? 1 : number + next;
        return number;
    }
};

let getNextFile: any;

const app = express();
app.use(bodyParser.json());

app.get('/', (req, res) => {
    getNextFile = memo();
    return res.send(`file:///${format(getNextFile())}`);
});
app.post('/', (req, res) => {
    const next = Number(req.query.next);
    if (!getNextFile) {
        getNextFile = memo();
    }
    return res.send(`file:///${format(getNextFile(next))}`);
});

app.use((req, res, next) => {
  next(createError(404));
});

const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
  console.log(`Api-gateway is listening on the port ${PORT}`);
});